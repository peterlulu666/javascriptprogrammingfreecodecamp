let countEL = document.getElementById("count-el")
let saveEL = document.getElementById("save-el")
let count = 0

function increment() {
    count = count + 1
    countEL.innerText = count.toString()
}

function save() {
    let countStr = count.toString() + " - "
    saveEL.textContent = saveEL.textContent + countStr
    countEL.innerText = 0
    count = 0
}