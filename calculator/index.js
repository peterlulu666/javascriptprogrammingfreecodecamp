let number1 = 20
let number2 = 10
let number1EL = document.getElementById("number1")
let number2EL = document.getElementById("number2")
number1EL.textContent = number1.toString()
number2EL.textContent = number2.toString()

function plus() {
    let compute = number1 + number2
    let plusEL = document.getElementById("compute")
    plusEL.textContent = compute.toString()
}

function subtract() {
    let compute = number1 - number2
    let computeEL = document.getElementById("compute")
    computeEL.textContent = compute.toString()
}

function divide() {
    let compute = number1 / number2
    let computeEL = document.getElementById("compute")
    computeEL.textContent = compute.toString()
}

function multiply() {
    let compute = number1 * number2
    let computeEL = document.getElementById("compute")
    computeEL.textContent = compute.toString()
}