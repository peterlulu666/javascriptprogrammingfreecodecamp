let sum = 0
let hasBlackJack = false
let isAlive = false
let message = ""
let cardList = []
let messageEL = document.getElementById("message-el")
let sumEL = document.getElementById("sum-el")
let cardEL = document.getElementById("card-el")

let player = {
    name: "Peter",
    chips: 200,
    sayHello: function () {
        console.log("Hello")
    }
}

let playerEL = document.getElementById("player-el")
playerEL.textContent = player.name + ": $" + player.chips
player.sayHello()

function renderGame(cardList) {
    let currentCard = ""
    for (let i = 0; i < cardList.length; ++i) {
        currentCard = currentCard + cardList[i].toString() + " "
    }
    cardEL.textContent = "card: " + currentCard
    for (let i = 0; i < cardList.length; ++i) {
        sum = sum + cardList[i]
    }
    sumEL.textContent = "sum: " + sum.toString()
    if (sum < 21) {
        message = "get another card"
    } else if (sum === 21) {
        message = "win"
        hasBlackJack = true
    } else {
        message = "out of game"
        isAlive = false
    }
    messageEL.textContent = message
}

function getRandomNumber() {
    let randomNumber = Math.floor(Math.random() * 13) + 1
    return randomNumber
}

function startGame() {
    isAlive = true
    let firstCard = getRandomNumber()
    let secondCard = getRandomNumber()
    cardList.push(firstCard, secondCard)
    renderGame(cardList)
}

function newCards() {
    if (isAlive === true && hasBlackJack === false) {
        let newCard = getRandomNumber()
        cardList.push(newCard)
        renderGame(cardList)
    }
}