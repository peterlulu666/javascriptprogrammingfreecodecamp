let myLead = []
const inputBtn = document.getElementById("input-btn")
const inputEL = document.getElementById("input-el")
const ulEL = document.getElementById("ul-el")
inputBtn.addEventListener("click", function () {
    // https://stackoverflow.com/questions/11563638/how-do-i-get-the-value-of-text-input-field-using-javascript
    myLead.push(inputEL.value)
    inputEL.value = ""
    showList()
})

function showList() {
    let listItem = ""
    for (let i = 0; i < myLead.length; ++i) {
        listItem = listItem + "<li><a target='_blank' href='" + myLead[i] + "'>" + myLead[i] + "</a></li>"
    }
    ulEL.innerHTML = listItem
}
